async function preInit(inputObj) {

}

async function postInit(inputObj) {
    console.log(`\n    ______ _                    
    |  _  (_)                   
    | | | |_ ___  ___ _   _ ____
    | | | | / __|/ __| | | |_  /
    | |/ /| \\__ \\ (__| |_| |/ / 
    |___/ |_|___/\\___|\\__,_/___|
                            
                            
                               `)
    console.log(`\n    Welcome to the start-discuz application
     This application requires to open these services: 
         FC : https://fc.console.aliyun.com/
         ACR: https://cr.console.aliyun.com/
     This application can help you quickly deploy the Discuz project:
         Full yaml configuration: https://github.com/devsapp/discuz#%E5%AE%8C%E6%95%B4yaml
         Discuz development docs: https://www.discuz.net/forum.php
     This application homepage: https://github.com/devsapp/start-discuz\n`)
}

module.exports = {
    postInit,
    preInit
}
